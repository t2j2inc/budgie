# README #

### Budgie ###

* Budgie is a budgeting application intended to allow users to help users to reach their financial goals.
* 0.0.0


### How do I get set up? ###

* The application is built for use on the android operating system.
* The application targets Android 4.4.4
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact