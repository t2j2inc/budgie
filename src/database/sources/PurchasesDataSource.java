package database.sources;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import classes.Purchase;
import database.databases.PurchasesSQLiteHelper;

public class PurchasesDataSource {

	private SQLiteDatabase database;
	private PurchasesSQLiteHelper dbHelper;
	private String[] allColumns = { PurchasesSQLiteHelper.COLUMN_ID,
			PurchasesSQLiteHelper.COLUMN_NAME,
			PurchasesSQLiteHelper.COLUMN_DATE,
			PurchasesSQLiteHelper.COLUMN_AMOUNT,
			PurchasesSQLiteHelper.COLUMN_CATEGORY,

			PurchasesSQLiteHelper.COLUMN_DESCRIPTION };
	String error = "";

	public PurchasesDataSource(Context context) {
		dbHelper = new PurchasesSQLiteHelper(context);
	}

	public void open() throws SQLException {
		database = dbHelper.getWritableDatabase();
	}

	public void close() {
		dbHelper.close();
	}

	public Purchase createPurchase(String name, String date,
			String category, double amount, String description) {

		ContentValues values = new ContentValues();
		values.put(PurchasesSQLiteHelper.COLUMN_NAME, name);
		values.put(PurchasesSQLiteHelper.COLUMN_DATE, date);
		values.put(PurchasesSQLiteHelper.COLUMN_AMOUNT, "" + amount);
		values.put(PurchasesSQLiteHelper.COLUMN_CATEGORY, category);

		values.put(PurchasesSQLiteHelper.COLUMN_DESCRIPTION, description);

		long insertId = database.insert(PurchasesSQLiteHelper.TABLE_PURCHASES,
				null, values);

		Cursor cursor = database.query(PurchasesSQLiteHelper.TABLE_PURCHASES,
				allColumns, PurchasesSQLiteHelper.COLUMN_ID + " = " + insertId,
				null, null, null, null);

		cursor.moveToFirst();

		Purchase newPurchase = cursorToPurchase(cursor);

		cursor.close();

		return newPurchase;

	}

	public List<Purchase> getAllPurchases() {
		List<Purchase> Purchases = new ArrayList<Purchase>();

		Cursor cursor = database.query(PurchasesSQLiteHelper.TABLE_PURCHASES,
				allColumns, null, null, null, null, null);
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			Purchase purchase = cursorToPurchase(cursor);
			Purchases.add(purchase);
			cursor.moveToNext();
		}
		cursor.close();
		return Purchases;
	}

	public void deletePurchase(Purchase purchase) {
		long id = purchase.getId();
		database.delete(PurchasesSQLiteHelper.TABLE_PURCHASES,
				PurchasesSQLiteHelper.COLUMN_ID + " = " + id, null);

	}

	private Purchase cursorToPurchase(Cursor cursor) {
		Purchase purchase = new Purchase();
		purchase.setId(cursor.getLong(0));
		purchase.setName(cursor.getString(1));
		purchase.setDate(cursor.getString(2));
		purchase.setAmount(cursor.getDouble(3));
		purchase.setCategory(cursor.getString(4));
		purchase.setDescription(cursor.getString(5));
		

		return purchase;
	}

}
