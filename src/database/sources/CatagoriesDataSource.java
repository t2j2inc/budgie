package database.sources;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import classes.Category;
import database.databases.CategorySQLiteHelper;

public class CatagoriesDataSource {

	private SQLiteDatabase database;
	private CategorySQLiteHelper dbHelper;
	private String[] allColumns = { CategorySQLiteHelper.COLUMN_ID,
			CategorySQLiteHelper.COLUMN_NAME };
	String error = "";

	public CatagoriesDataSource(Context context) {
		dbHelper = new CategorySQLiteHelper(context);
	}

	public void open() throws SQLException {
		database = dbHelper.getWritableDatabase();
	}

	public void close() {
		dbHelper.close();
	}

	public Category createCategory(String name) {

		ContentValues values = new ContentValues();

		values.put(CategorySQLiteHelper.COLUMN_NAME, name);

		long insertId = database.insert(CategorySQLiteHelper.TABLE_CATEGORIES,
				null, values);

		Cursor cursor = database.query(CategorySQLiteHelper.TABLE_CATEGORIES,
				allColumns, CategorySQLiteHelper.COLUMN_ID + " = " + insertId,
				null, null, null, null);

		cursor.moveToFirst();

		Category newCategory = cursorToCategory(cursor);

		cursor.close();

		return newCategory;

	}

	public List<Category> getAllCategories() {
		List<Category> catagories = new ArrayList<Category>();

		Cursor cursor = database.query(CategorySQLiteHelper.TABLE_CATEGORIES,
				allColumns, null, null, null, null, null);
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			Category category = cursorToCategory(cursor);
			catagories.add(category);
			cursor.moveToNext();
		}
		cursor.close();
		return catagories;
	}
	
	public void deleteCategory(Category category){
		long id = category.getId();
		database.delete(CategorySQLiteHelper.TABLE_CATEGORIES,CategorySQLiteHelper.COLUMN_ID + " = " + id , null);
		
	}

	private Category cursorToCategory(Cursor cursor) {
		Category category = new Category();
		category.setId(cursor.getLong(0));
		category.setName(cursor.getString(1));
		return category;
	}

}
