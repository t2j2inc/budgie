package database.sources;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import classes.PlannedExpense;
import database.databases.PlannedExpenseSQLiteHelper;

public class PlannedExpensesDataSource {
	private SQLiteDatabase database;
	private PlannedExpenseSQLiteHelper dbHelper;
	private String[] allColumns = { PlannedExpenseSQLiteHelper.COLUMN_ID,
			PlannedExpenseSQLiteHelper.COLUMN_CATEGORY,
			PlannedExpenseSQLiteHelper.COLUMN_AMOUNT };
	String error = "";

	public PlannedExpensesDataSource(Context context) {
		dbHelper = new PlannedExpenseSQLiteHelper(context);
	}

	public void open() throws SQLException {
		database = dbHelper.getWritableDatabase();
	}

	public void close() {
		dbHelper.close();
	}

	public PlannedExpense createPlannedExpense(String category, double amount) {

		ContentValues values = new ContentValues();

		values.put(PlannedExpenseSQLiteHelper.COLUMN_CATEGORY, category);
		values.put(PlannedExpenseSQLiteHelper.COLUMN_AMOUNT, "" + amount);

		long insertId = database
				.insert(PlannedExpenseSQLiteHelper.TABLE_PLANNED_EXPENSES,
						null, values);

		Cursor cursor = database.query(
				PlannedExpenseSQLiteHelper.TABLE_PLANNED_EXPENSES, allColumns,
				PlannedExpenseSQLiteHelper.COLUMN_ID + " = " + insertId, null,
				null, null, null);

		cursor.moveToFirst();

		PlannedExpense newPlannedExpense = cursorToPlannedExpense(cursor);

		cursor.close();

		return newPlannedExpense;

	}
	
	public PlannedExpense createPlannedExpense(long id ,String category, double amount) {

		ContentValues values = new ContentValues();

		values.put(PlannedExpenseSQLiteHelper.COLUMN_ID, id);
		values.put(PlannedExpenseSQLiteHelper.COLUMN_CATEGORY, category);
		values.put(PlannedExpenseSQLiteHelper.COLUMN_AMOUNT, "" + amount);

		long insertId = database
				.insert(PlannedExpenseSQLiteHelper.TABLE_PLANNED_EXPENSES,
						null, values);

		Cursor cursor = database.query(
				PlannedExpenseSQLiteHelper.TABLE_PLANNED_EXPENSES, allColumns,
				PlannedExpenseSQLiteHelper.COLUMN_ID + " = " + insertId, null,
				null, null, null);

		cursor.moveToFirst();

		PlannedExpense newPlannedExpense = cursorToPlannedExpense(cursor);

		cursor.close();

		return newPlannedExpense;

	}

	public List<PlannedExpense> getAllPlannedExpenses() {
		List<PlannedExpense> plannedExpenses = new ArrayList<PlannedExpense>();

		Cursor cursor = database.query(
				PlannedExpenseSQLiteHelper.TABLE_PLANNED_EXPENSES, allColumns,
				null, null, null, null, null);
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			PlannedExpense plannedExpense = cursorToPlannedExpense(cursor);
			plannedExpenses.add(plannedExpense);
			cursor.moveToNext();
		}
		cursor.close();
		return plannedExpenses;
	}

	public void deletePlannedExpense(PlannedExpense plannedExpense) {
		long id = plannedExpense.getId();
		database.delete(PlannedExpenseSQLiteHelper.TABLE_PLANNED_EXPENSES,
				PlannedExpenseSQLiteHelper.COLUMN_ID + " = " + id, null);

	}

	private PlannedExpense cursorToPlannedExpense(Cursor cursor) {
		PlannedExpense plannedExpense = new PlannedExpense();
		plannedExpense.setId(cursor.getLong(0));
		plannedExpense.setCategory(cursor.getString(1));
		plannedExpense.setAmount(cursor.getDouble(2));
		return plannedExpense;
	}
}
