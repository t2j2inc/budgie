package tabelements;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import operations.adapters.ReportAdapter;
import android.app.ListFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;
import classes.Category;
import classes.PlannedExpense;
import classes.Purchase;

import com.t2j2budgie.R;

import database.sources.CatagoriesDataSource;
import database.sources.PlannedExpensesDataSource;
import database.sources.PurchasesDataSource;

public class ReportFragment extends ListFragment  {

	private List<String> reports;
	private ReportAdapter adapter;
	private List<Purchase> purchases;
	private double total = 0;
	private List<Category> categories;
	private List<Double> values;
	private List<PlannedExpense>plannedExpenses;

	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.report_tab_layout, container,
				false);
		reports = new ArrayList<String>();
		PurchasesDataSource purchasesDataSource = new PurchasesDataSource(
				view.getContext());
		purchasesDataSource.open();
		purchases = purchasesDataSource.getAllPurchases();
		CatagoriesDataSource categoriesDataSource = new CatagoriesDataSource(
				view.getContext());
		categoriesDataSource.open();
		categories = categoriesDataSource.getAllCategories();
	    values = new ArrayList<Double>();
		ArrayList<String> labels = new ArrayList<String>();
		for (int i = 0; i < categories.size(); i++) {
			labels.add(categories.get(i).getName());

			values.add(0.0);
		}
		for (Purchase p : purchases) {
			for (int i = 0; i < categories.size(); i++) {
				if (p.getCategory().equals(categories.get(i).getName())) {
					values.set(i, values.get(i) + p.getAmount());

				}
			}
			total += p.getAmount();
		}
		PlannedExpensesDataSource plannedExpensesDataSource = new PlannedExpensesDataSource(
				getActivity());
		plannedExpensesDataSource.open();
		plannedExpenses = plannedExpensesDataSource
				.getAllPlannedExpenses();
		int valperc = 0;
		for (int i = 0; i < plannedExpenses.size(); i++) {
			valperc += plannedExpenses.get(i).getAmount();
		}
		for (int i = 0; i < plannedExpenses.size(); i++) {
			double val = values.get(i);

			if ((int) (plannedExpenses.get(i).getAmount() / valperc * 100) < (int) ((val / total) * 100)) {

				reports.add(categories.get(i).getName());
			}
		}
		adapter = new ReportAdapter(getActivity(), reports);
		setListAdapter(adapter);
		return view;
	}

	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
		super.onListItemClick(l, v, position, id);
		String category = reports.get(position);
		NumberFormat numberFormat = NumberFormat.getCurrencyInstance();
		for(int i = 0; i < plannedExpenses.size(); i++){
			if(category.equals(plannedExpenses.get(i).getCategory())){
				Toast.makeText(getActivity(), numberFormat.format(values.get(i)) + " spent on " + categories.get(i), Toast.LENGTH_SHORT).show();
			}
			
		}
	}
	
	
	
}