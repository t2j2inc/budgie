package tabelements;

import com.t2j2budgie.R;

import android.app.ActionBar.Tab;
import android.app.ActionBar;
import android.app.Fragment;
import android.app.FragmentTransaction;

public class ReportTabListener implements ActionBar.TabListener{

	Fragment fragment;
	
	public ReportTabListener(Fragment fragment){
		this.fragment = fragment;
	}
	
	 public void onTabSelected(Tab tab, FragmentTransaction ft) {
		 if(tab.getPosition() == 1){
			 ReportFragment fragmentList = new ReportFragment();
			 ft.replace(R.id.fragment_container, fragmentList);
		 }
			ft.replace(R.id.fragment_container, fragment);
		}
		
		public void onTabUnselected(Tab tab, FragmentTransaction ft) {
			ft.remove(fragment);
		}
		
		public void onTabReselected(Tab tab, FragmentTransaction ft) {
			 
		}
	}