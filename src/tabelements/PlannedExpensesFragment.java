package tabelements;

import java.util.ArrayList;
import java.util.List;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import classes.PlannedExpense;

import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.t2j2budgie.R;

import database.sources.PlannedExpensesDataSource;

public class PlannedExpensesFragment extends Fragment {

	private PieChart plannedExpenseChart;
	private ArrayList<Entry> entries;
	private ArrayList<Integer> colors;
	private ArrayList<String> categoryNames;
	private PieDataSet dataSet;
	private PieData data;
	private List<PlannedExpense> plannedExpenses;

	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.planned_expense_tab_layout,
				container, false);
		plannedExpenseChart = (PieChart) view.findViewById(R.id.chart1);

		plannedExpenseChart.setHoleRadius(60f);
		plannedExpenseChart.setDescription("");
		plannedExpenseChart.setDrawYValues(true);

		plannedExpenseChart.setRotationAngle(0);

		plannedExpenseChart.setDrawXValues(true);

		plannedExpenseChart.setRotationEnabled(true);

		plannedExpenseChart.setUsePercentValues(true);
		
		colors = new ArrayList<Integer>();

		
     
		createColors();
		PlannedExpensesDataSource plannedExpensesDataSource = new PlannedExpensesDataSource(view.getContext());
		plannedExpensesDataSource.open();
		plannedExpenses = plannedExpensesDataSource.getAllPlannedExpenses();
		entries = new ArrayList<Entry>();
		categoryNames = new ArrayList<String>();
		for(int index = 0; index < plannedExpenses.size(); index++){
			entries.add(new Entry((float)plannedExpenses.get(index).getAmount(),index));
			categoryNames.add(plannedExpenses.get(index).getCategory());
		}
		dataSet = new PieDataSet(entries, "");
		dataSet.setColors(colors);
		dataSet.setSliceSpace(3f);
		data = new PieData(categoryNames,dataSet);
		plannedExpenseChart.setData(data);
	

		

		 plannedExpenseChart.highlightValues(null);
		plannedExpenseChart.invalidate();
		return view;
	}

	private void createColors() {
		for (int c : ColorTemplate.VORDIPLOM_COLORS)
			colors.add(c);
		for (int c : ColorTemplate.JOYFUL_COLORS)
			colors.add(c);
		for (int c : ColorTemplate.COLORFUL_COLORS)
			colors.add(c);
		for (int c : ColorTemplate.LIBERTY_COLORS)
			colors.add(c);
		for (int c : ColorTemplate.PASTEL_COLORS)
			colors.add(c);
	}
}