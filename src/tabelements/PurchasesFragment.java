package tabelements;

import android.app.Fragment;
import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import classes.Category;
import classes.Purchase;

import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.t2j2budgie.R;

import database.sources.CatagoriesDataSource;
import database.sources.PurchasesDataSource;

public class PurchasesFragment extends Fragment {
	private List<Purchase> purchases;
	private PieChart pieChart;
	private ArrayList<Integer> colors;

	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.purchases_tab_layout, container,
				false);
		PurchasesDataSource purchasesDataSource = new PurchasesDataSource(
				view.getContext());
		purchasesDataSource.open();
		purchases = purchasesDataSource.getAllPurchases();
		CatagoriesDataSource categoriesDataSource = new CatagoriesDataSource(
				view.getContext());
		categoriesDataSource.open();
		List<Category> categories = categoriesDataSource.getAllCategories();
		List<Double> values = new ArrayList<Double>();
		ArrayList<String> labels = new ArrayList<String>();
		for (int i = 0; i < categories.size(); i++){
			labels.add(categories.get(i).getName());
		
			values.add(0.0);
		}
		for (Purchase p : purchases) {
			for (int i = 0; i < categories.size(); i++) {
				if (p.getCategory().equals(categories.get(i).getName())) {
					values.set(i, values.get(i) + p.getAmount());
				}
			}
		}
		pieChart = (PieChart) view.findViewById(R.id.chart1);

		pieChart.setHoleRadius(60f);
		pieChart.setDescription("");
		pieChart.setDrawYValues(true);

		pieChart.setRotationAngle(0);

		pieChart.setDrawXValues(true);

		pieChart.setRotationEnabled(true);

		pieChart.setUsePercentValues(true);

		colors = new ArrayList<Integer>();
		
		ArrayList<Entry> entries = new ArrayList<Entry>();
		for(int index = 0; index < values.size(); index++){
			double d = values.get(index);
			entries.add(new Entry((float)d , index));
		}
        PieDataSet dataSet = new PieDataSet(entries, "");
        dataSet.setSliceSpace(3f);
        dataSet.setColors(colors);
        PieData data = new PieData(labels ,dataSet);
        pieChart.setData(data);
		createColors();
		return view;
	}

	private void createColors() {
		for (int c : ColorTemplate.VORDIPLOM_COLORS)
			colors.add(c);
		for (int c : ColorTemplate.JOYFUL_COLORS)
			colors.add(c);
		for (int c : ColorTemplate.COLORFUL_COLORS)
			colors.add(c);
		for (int c : ColorTemplate.LIBERTY_COLORS)
			colors.add(c);
		for (int c : ColorTemplate.PASTEL_COLORS)
			colors.add(c);
	}
}