package activities;

import java.util.List;

import operations.ScreenEditor;
import operations.adapters.PlanExpensesAdapter;
import android.app.ListActivity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import classes.Category;

import com.t2j2budgie.R;

import database.sources.CatagoriesDataSource;

public class PlannedExpensesActivity extends ListActivity implements
		OnClickListener {

	private PlanExpensesAdapter adapter;
	private List<Category> categories;
	private CatagoriesDataSource categoriesDataSource;
	private ScreenEditor screenEditor;
	private ImageButton back;
	private TextView total;
	private Button save;

	public TextView getTotal() {
		return total;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		screenEditor = new ScreenEditor(this);
		screenEditor.fullScreen();
		setContentView(R.layout.planned_expenses);
		total = (TextView) findViewById(R.id.tv_total);
		save = (Button) findViewById(R.id.b_save);
		save.setOnClickListener(this);
		categoriesDataSource = new CatagoriesDataSource(this);
		categoriesDataSource.open();
		categories = categoriesDataSource.getAllCategories();
		adapter = new PlanExpensesAdapter(this, categories);
		setListAdapter(adapter);
		back = (ImageButton) findViewById(R.id.ib_back);
		back.setOnClickListener(this);

	}

	@Override
	public void onClick(View v) {

		finish();
	}

	@Override
	protected void onPause() {
		super.onPause();

		finish();
	}

}
