package activities;

import java.util.List;

import operations.ScreenEditor;
import operations.adapters.PurchasesAdapter;
import android.app.ListActivity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;
import classes.Purchase;

import com.t2j2budgie.R;

import database.sources.PurchasesDataSource;

public class PurchaseListActivity extends ListActivity implements
		OnClickListener {
	private PurchasesAdapter purchasesAdapter;
	private List<Purchase> purchases;
	private PurchasesDataSource purchasesDataSource;
	private ImageButton backImageButton;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		ScreenEditor screenEditor = new ScreenEditor(this);
		screenEditor.fullScreen();

		setContentView(R.layout.purchases_list);

		purchasesDataSource = new PurchasesDataSource(this);

		purchasesDataSource.open();
		purchases = purchasesDataSource.getAllPurchases();
		purchasesAdapter = new PurchasesAdapter(this, purchases);

		backImageButton = (ImageButton) findViewById(R.id.ib_back);
		backImageButton.setOnClickListener(this);
		setListAdapter(purchasesAdapter);

	}

	@Override
	public void onClick(View v) {
		finish();
	}

	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		super.onListItemClick(l, v, position, id);
		Toast.makeText(this, purchases.get(position).getDescription(),
				Toast.LENGTH_SHORT).show();

	}

	@Override
	protected void onPause() {
		super.onPause();
		finish();
	}

}
