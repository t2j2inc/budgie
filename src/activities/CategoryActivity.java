package activities;

import java.util.ArrayList;
import java.util.List;

import operations.ScreenEditor;
import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.Toast;
import classes.Category;
import classes.PlannedExpense;
import classes.Purchase;

import com.t2j2budgie.R;

import database.sources.CatagoriesDataSource;
import database.sources.PlannedExpensesDataSource;
import database.sources.PurchasesDataSource;

public class CategoryActivity extends Activity implements View.OnClickListener {

	private ImageButton back;
	private ImageButton submit;
	private EditText ed;
	private ImageButton delete;
	private Spinner deletespinner;
	private CatagoriesDataSource catagoriesDataSource;
	@Override
	protected void onPause() {
		super.onPause();
		finish();
	}

	private List<Category> categories;
	private List<String> categoryNames;
	private ArrayAdapter<String> dataAdapter;
	private ScreenEditor screenEditor;
	private PlannedExpensesDataSource plannedExpensesDataSource;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		screenEditor = new ScreenEditor(this);
		screenEditor.fullScreen();
		setContentView(R.layout.category);
		back = (ImageButton) findViewById(R.id.ib_back);
		submit = (ImageButton) findViewById(R.id.ib_submit);
		ed = (EditText) findViewById(R.id.et_Category);
		back.setOnClickListener(this);
		submit.setOnClickListener(this);
		new Category();
		catagoriesDataSource = new CatagoriesDataSource(this);
		catagoriesDataSource.open();
		plannedExpensesDataSource = new PlannedExpensesDataSource(this);
		plannedExpensesDataSource.open();
		delete = (ImageButton) findViewById(R.id.ib_del);
		delete.setOnClickListener(this);
		deletespinner = (Spinner) findViewById(R.id.sp_del);
		categories = catagoriesDataSource.getAllCategories();
		categoryNames = new ArrayList<String>();
		getCategoryNames();
		dataAdapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, categoryNames);
		dataAdapter
				.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
		deletespinner.setAdapter(dataAdapter);
	}

	@Override
	public void onClick(View v) {

		switch (v.getId()) {
		case R.id.ib_back:
			finish();
			break;
		case R.id.ib_submit:
			String catagoryName = ed.getText().toString();
			if (catagoryName.length() == 0) {
				Toast.makeText(this, "Must enter a category name",
						Toast.LENGTH_SHORT).show();
			} /*
			 * else if (categories.size() == 3) { Toast.makeText( this,
			 * "Functionality for greater than 3 categories not yet supported",
			 * Toast.LENGTH_SHORT).show(); }
			 */else {

				catagoriesDataSource.createCategory(catagoryName);
				Toast.makeText(this, "Category Created: " + catagoryName,
						Toast.LENGTH_LONG).show();
				plannedExpensesDataSource.createPlannedExpense(catagoryName, 0);
			
				finish();

			}
			break;
		case R.id.ib_del:
			if (categoryNames.size() > 0) {
				int deleteIndex = deletespinner.getSelectedItemPosition();
				catagoriesDataSource
						.deleteCategory(categories.get(deleteIndex));
				Toast.makeText(this,
						"Category deleted: " + categoryNames.get(deleteIndex),
						Toast.LENGTH_LONG).show();
				List<PlannedExpense> expenses = plannedExpensesDataSource
						.getAllPlannedExpenses();
				PlannedExpense plannedExpense = expenses.get(deleteIndex);
				plannedExpensesDataSource.deletePlannedExpense(plannedExpense);
				PurchasesDataSource purchasesDataSource = new PurchasesDataSource(
						this);
				purchasesDataSource.open();
				List<Purchase> purchases = purchasesDataSource
						.getAllPurchases();
				for (Purchase p : purchases) {
					if (p.getCategory().equals(categoryNames.get(deleteIndex))) {
						purchasesDataSource.deletePurchase(p);
					}
				}
				
				finish();
				break;
			}
		}

	}

	private void getCategoryNames() {
		for (Category catagory : categories) {
			categoryNames.add(catagory.getName());
		}
	}

}
