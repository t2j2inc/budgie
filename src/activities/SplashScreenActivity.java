package activities;

import operations.ScreenEditor;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.widget.TextView;

import com.t2j2budgie.R;

public class SplashScreenActivity extends Activity {
	private TextView title;
	private Typeface typeFace;
	private Intent mainMenuIntent;
	private MediaPlayer chirpingBirds;
	private ScreenEditor screenEditor;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		screenEditor = new ScreenEditor(this);
		screenEditor.fullScreen();
		setContentView(R.layout.splash_screen);

		title = (TextView) findViewById(R.id.tv_splash_screen_title);
		typeFace = Typeface.createFromAsset(getAssets(), "fonts/TREAI___.TTF");
		title.setTypeface(typeFace);
		mainMenuIntent = new Intent(this, MainMenuActivity.class);
		chirpingBirds = MediaPlayer.create(this, R.raw.cardinal);
		chirpingBirds.start();
	

		Thread timer = new Thread() {
			public void run() {
				try {
					sleep(3000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				} finally {
					startActivity(mainMenuIntent);
				}
			}
		};
		timer.start();

	}

	@Override
	protected void onPause() {
		super.onPause();
		chirpingBirds.release();
		finish();
	}

}
