package activities;

import operations.ScreenEditor;

import com.t2j2budgie.R;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

public class MainMenuActivity extends Activity implements OnClickListener {
	private ImageButton closeApplication;
	private Button categorybutton;
	private Button purchasesbutton;
	private Button plannedexpensesbutton;
	private Button report;
	private Button purchaseListButton;
	private TextView title;
	private Typeface titleFont;
	private ScreenEditor screenEditor;

	private void init() {
		closeApplication = (ImageButton) findViewById(R.id.ib_closeApp);
		closeApplication.setOnClickListener(this);
		title = (TextView) findViewById(R.id.tv_menuHeading);
		titleFont = Typeface.createFromAsset(getAssets(), "fonts/fast99.ttf");
		title.setTypeface(titleFont);
		categorybutton = (Button) findViewById(R.id.button2);
		categorybutton.setOnClickListener(this);
		purchasesbutton = (Button) findViewById(R.id.button3);
		purchasesbutton.setOnClickListener(this);
		plannedexpensesbutton = (Button) findViewById(R.id.button4);
		plannedexpensesbutton.setOnClickListener(this);
		report = (Button) findViewById(R.id.button5);
		report.setOnClickListener(this);
		purchaseListButton = (Button) findViewById(R.id.button6);
		purchaseListButton.setOnClickListener(this);

	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		screenEditor = new ScreenEditor(this);
		screenEditor.fullScreen();
		setContentView(R.layout.main_menu);
		init();

	}

	@Override
	public void onClick(View v) {
		Intent i;
		switch (v.getId()) {
		case R.id.button2:
			i = new Intent(this, CategoryActivity.class);
			startActivity(i);
			
			break;
		case R.id.button3:
			i = new Intent(this, PurchasesActivity.class);
			startActivity(i);
			
			break;
		case R.id.button4:
			i = new Intent(this, PlannedExpensesActivity.class);
			startActivity(i);
			
			break;
		case R.id.button5:
			i = new Intent(this, ReportActivity.class);
			startActivity(i);
			
			break;
		case R.id.button6:
			i = new Intent(this, PurchaseListActivity.class);
			startActivity(i);
		
			break;
		case R.id.ib_closeApp:
			finish();
			break;
		}

	}
}
