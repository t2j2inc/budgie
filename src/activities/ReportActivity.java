package activities;

import tabelements.PlannedExpensesFragment;
import tabelements.PurchasesFragment;
import tabelements.ReportFragment;
import tabelements.ReportTabListener;
import android.app.ActionBar;
import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;

import com.t2j2budgie.R;

public class ReportActivity extends Activity implements OnClickListener {
	private ActionBar.Tab tab1, tab2, tab3;
	private Fragment fragmentTab1 = new PlannedExpensesFragment();
	private Fragment fragmentTab2 = new ReportFragment();
	private Fragment fragmentTab3 = new PurchasesFragment();
	private ImageButton backImageButton;

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.maintablayout);

		ActionBar actionBar = getActionBar();

		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
		tab1 = actionBar.newTab().setText("Planned Expenses");
		tab2 = actionBar.newTab().setText("Report");
		tab3 = actionBar.newTab().setText("Purchases");
		tab1.setTabListener(new ReportTabListener(fragmentTab1));
		tab2.setTabListener(new ReportTabListener(fragmentTab2));
		tab3.setTabListener(new ReportTabListener(fragmentTab3));
		actionBar.addTab(tab1);
		actionBar.addTab(tab2);
		actionBar.addTab(tab3);
		backImageButton = (ImageButton) findViewById(R.id.ib_back);
		backImageButton.setOnClickListener(this);

	}

	@Override
	public void onClick(View v) {
		finish();
	}

	@Override
	protected void onPause() {
		super.onPause();
		finish();
	}
}