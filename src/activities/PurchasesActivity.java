package activities;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import operations.ScreenEditor;
import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.Toast;
import classes.Category;

import com.t2j2budgie.R;

import database.sources.CatagoriesDataSource;
import database.sources.PurchasesDataSource;

public class PurchasesActivity extends Activity implements View.OnClickListener {

	private ImageButton back;
	private ImageButton submit;
	private EditText Pname;
	private EditText Pvalue;
	private EditText Pdescription;
	private Spinner catagorySpinner;
	private List<String> catagoryNames;
	private List<Category> catagories;
	private ArrayAdapter<String> dataAdapter;
	private CatagoriesDataSource catagoriesDataSource;
	private ScreenEditor screenEditor;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		screenEditor = new ScreenEditor(this);
		screenEditor.fullScreen();
		setContentView(R.layout.purchases);
		back = (ImageButton) findViewById(R.id.ib_back);
		submit = (ImageButton) findViewById(R.id.ib_submit);
		Pname = (EditText) findViewById(R.id.et_PurchasesName);
		Pvalue = (EditText) findViewById(R.id.et_PurchasesValue);
		Pdescription = (EditText) findViewById(R.id.et_PurchasesDescription);
		catagorySpinner = (Spinner) findViewById(R.id.sp_PurchasesCategory);
		catagoriesDataSource = new CatagoriesDataSource(this);
		catagoriesDataSource.open();
		catagories = catagoriesDataSource.getAllCategories();
		catagoryNames = new ArrayList<String>();
		getCategoryNames();
		dataAdapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, catagoryNames);
		dataAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		catagorySpinner.setAdapter(dataAdapter);
		back.setOnClickListener(this);
		submit.setOnClickListener(this);

	}

	private void getCategoryNames() {
		for (Category catagory : catagories) {
			catagoryNames.add(catagory.getName());
		}
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.ib_back:
			finish();
			break;
		case R.id.ib_submit:
			String purchasename = Pname.getText().toString();
			String purchasevalue = Pvalue.getText().toString();
			String purchasedescription = Pdescription.getText().toString();
			SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy",
					Locale.US);
			String category = catagorySpinner.getSelectedItem().toString();
			Log.d("Travis", category);
			Date date = new Date();
			String dateString = dateFormat.format(date);
			if (purchasename.length() == 0) {
				Toast.makeText(this, "Must enter purchase name",
						Toast.LENGTH_SHORT).show();
			} else if (purchasevalue.length() == 0) {
				Toast.makeText(this, "Must enter purchase value",
						Toast.LENGTH_SHORT).show();
			} else {
				PurchasesDataSource purchasesDataSource = new PurchasesDataSource(
						this);
				purchasesDataSource.open();
				purchasesDataSource.createPurchase(purchasename, dateString,
						category, Double.parseDouble(purchasevalue),
						purchasedescription);
				purchasesDataSource.close();
				finish();
			}
			break;
		}
	}

	@Override
	protected void onPause() {
		super.onPause();
		finish();
	}
}
