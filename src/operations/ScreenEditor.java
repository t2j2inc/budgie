package operations;

import android.app.Activity;
import android.view.Window;
import android.view.WindowManager;

public class ScreenEditor {
	private Activity a;

	public ScreenEditor(Activity a) {
		this.a = a;
	}

	public void fullScreen() {
		a.requestWindowFeature(Window.FEATURE_NO_TITLE);
		a.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
	}
}
