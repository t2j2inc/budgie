package operations.adapters;

import java.text.NumberFormat;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import classes.Purchase;

import com.t2j2budgie.R;

public class PurchasesAdapter extends BaseAdapter {

	private Activity activity;
	private List<Purchase> purchases;
	private static LayoutInflater inflater = null;
	private int row;
	private TextView nameTextView;
	private TextView dateTextView;
	private TextView amountTextView;
	private TextView categoryTextView;

	public PurchasesAdapter(Activity a, List<Purchase> p) {
		activity = a;
		purchases = p;
		inflater = (LayoutInflater) activity
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		row = R.layout.purchase_row;
	}

	@Override
	public int getCount() {
		return purchases.size();
	}

	@Override
	public Object getItem(int position) {
		return position;
	}

	@Override
	public long getItemId(int position) {
		return purchases.get(position).getId();
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View view = convertView;
		if (convertView == null)
			view = inflater.inflate(row, null);
		Purchase item = purchases.get(position);
		nameTextView = (TextView) view.findViewById(R.id.tv_name);
		
		dateTextView = (TextView) view.findViewById(R.id.tv_date);
		
		categoryTextView = (TextView) view.findViewById(R.id.tv_category);

		amountTextView = (TextView) view.findViewById(R.id.tv_amount);
		
		nameTextView.setText(item.getName());
		
		dateTextView.setText(item.getDate());
		NumberFormat numberFormat  = NumberFormat.getCurrencyInstance();
		String value = numberFormat.format(item.getAmount());
		
		amountTextView.setText(value);
	
		categoryTextView.setText(item.getCategory());
		return view;
	}



}