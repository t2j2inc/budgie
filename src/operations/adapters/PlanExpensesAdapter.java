package operations.adapters;

import java.util.ArrayList;
import java.util.List;

import activities.PlannedExpensesActivity;
import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;
import classes.Category;
import classes.PlannedExpense;

import com.t2j2budgie.R;

import database.sources.PlannedExpensesDataSource;

public class PlanExpensesAdapter extends BaseAdapter implements
		OnSeekBarChangeListener {

	private Activity activity;
	private List<Category> categories;
	private static LayoutInflater inflater = null;
	private int row;
	private EditText valueEditText;
	private SeekBar setterSeekBar;
	private List<EditText> values;
	private TextView categoryNameTextView;

	public PlanExpensesAdapter(Activity a, List<Category> c) {
		activity = a;
		categories = c;
		inflater = (LayoutInflater) activity
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		row = R.layout.planned_expense_row;
		values = new ArrayList<EditText>();

	}

	@Override
	public int getCount() {
		return categories.size();
	}

	@Override
	public Object getItem(int position) {
		return position;
	}

	@Override
	public long getItemId(int position) {
		return categories.get(position).getId();
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View view = convertView;
		if (convertView == null)
			view = inflater.inflate(row, null);
		Category item = categories.get(position);
		categoryNameTextView = (TextView) view
				.findViewById(R.id.tv_category_name);
		categoryNameTextView.setTag(position);
		valueEditText = (EditText) view.findViewById(R.id.et_value);
		setterSeekBar = (SeekBar) view.findViewById(R.id.sb_setValue);

		valueEditText.setTag(position);
		setterSeekBar.setTag(position);
		PlannedExpensesDataSource e = new PlannedExpensesDataSource(activity);
		e.open();
		PlannedExpense expense = e.getAllPlannedExpenses().get(position);
		List<PlannedExpense> expenses = e.getAllPlannedExpenses();
		int sum = 0;
		for (PlannedExpense index : expenses) {
			sum += index.getAmount();
		}
		PlannedExpensesActivity a = (PlannedExpensesActivity) activity;
		a.getTotal().setText("Total: " + sum);
		valueEditText.setText("" + (int) expense.getAmount());
		setterSeekBar.setProgress((int) expense.getAmount());
		setterSeekBar.setOnSeekBarChangeListener(this);
		categoryNameTextView.setTag(position);
		categoryNameTextView.setText(item.getName());
		values.add(valueEditText);

		return view;
	}

	@Override
	public void onProgressChanged(SeekBar seekBar, int progress,
			boolean fromUser) {
		int position = (Integer) seekBar.getTag();

		PlannedExpensesDataSource e = new PlannedExpensesDataSource(activity);
		e.open();
		List<PlannedExpense> plannedExpenses = e.getAllPlannedExpenses();

		PlannedExpense expense = plannedExpenses.get(position);
		expense.setAmount(progress);
		e.deletePlannedExpense(expense);
		e.createPlannedExpense(expense.getId(), expense.getCategory(),
				expense.getAmount());
		values.get(position).setText("" + progress);
		int sum = 0;
		List<PlannedExpense> expenses = e.getAllPlannedExpenses();
		for (PlannedExpense index : expenses) {
			sum += index.getAmount();
		}
		PlannedExpensesActivity a = (PlannedExpensesActivity) activity;
		a.getTotal().setText("Total: " + sum);
		Log.d("Travis", ""
				+ e.getAllPlannedExpenses().get(position).getAmount());
	}

	@Override
	public void onStartTrackingTouch(SeekBar seekBar) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onStopTrackingTouch(SeekBar seekBar) {

	}

}
