package operations.adapters;

import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.t2j2budgie.R;

public class ReportAdapter extends BaseAdapter{
	private Activity activity;
	private static LayoutInflater inflater = null;
	private int row;
	private TextView reportTextView;
	private List<String> reports;
	
	public ReportAdapter(Activity a, List<String> r) {
		activity = a;
		reports = r;
		inflater = (LayoutInflater) activity
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		row = R.layout.report_row;
	}

	@Override
	public int getCount() {
		return reports.size();
	}

	@Override
	public Object getItem(int position) {
		return position;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View view = convertView;
		if (convertView == null)
			view = inflater.inflate(row, null);
		String item = reports.get(position);
		reportTextView = (TextView) view.findViewById(R.id.tv_report);
		
		reportTextView.setText("Over Spending on " + item);
		return view;
	}

}
